#!/bin/bash
echo \#####################################################;
echo \#  Running sonar commande to analyse JS project \#;
echo \#####################################################;
sonar-scanner   -Dsonar.organization=vuejs-cours   -Dsonar.projectKey=vuejs_cours_vue.js_cours_linux   -Dsonar.sources=.   -Dsonar.host.url=https://sonarcloud.io
